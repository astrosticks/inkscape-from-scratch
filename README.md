# 🏔️ Inkscape from Scratch 😺 

Tools and tricks to help create assets for [Scratch](https://scratch.mit.edu) projects using [Inkscape](https://inkscape.org). 

Created by [David Gene](https://astrosticks.com), a.k.a. [Astro947](https://scratch.mit.edu/users/Astro947/).
